#! python
import os
import sys
import re
import codecs

def find_str(rootdir,pattern):
    l = list(os.walk(rootdir))
    p = re.compile(pattern)
    for path,dir_list,file_list in l:
        for file_name in file_list:
            name = os.path.join(path, file_name)
            try:
                with codecs.open(name) as f:
                    c = f.read()
                    if p.search(c) is not None:
                        print(name)
            except:
                pass
if __name__ =='__main__':
    if len(sys.argv) != 3:
        print("Usage: python3 find_str_in_folder.py <source folder> <target>")
    else:
        source_folder = sys.argv[1]
        target = sys.argv[2]
        print("find {0} form {1}".format(target,source_folder))
        find_str(source_folder,target)