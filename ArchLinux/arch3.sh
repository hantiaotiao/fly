#! /bin/bash

function func_write_log()
{
    var_str=$1
    var_curr_timestamp=`date "+%Y-%m-%d %H:%M:%S"`
    var_str="\n\033[32m${var_curr_timestamp} ${var_str}\033[0m"
    echo -e "${var_str}"
}

#func_write_log "install desktop start"
#pacman -Sy xorg xorg-server xorg-xinit xorg-apps xterm
#pacman -Sy plasma kde-applications
#pacman -Sy lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings
#systemctl enable lightdm.service 
#func_write_log "install desktop end"

# https://zhuanlan.zhihu.com/p/457315257
func_write_log "install desktop start"
pacman -Sy xorg sddm plasma  konsole
# pacman -Sy kde-applications # 这个软件包中有很多用不到的软件
systemctl enable sddm
func_write_log "install desktop end"

func_write_log "install chinese support start"
pacman -Sy   adobe-source-han-serif-cn-fonts adobe-source-han-sans-cn-fonts
sudo pacman -S fcitx5 fcitx5-chinese-addons fcitx5-qt fcitx5-gtk
func_write_log "install chinese support start"


