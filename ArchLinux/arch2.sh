#! /bin/bash

# your username and password, edit i
USER="han"
PASSWORD="123456"
ROOTPASSWORD="123456"

function func_write_log()
{
    var_str=$1
    var_curr_timestamp=`date "+%Y-%m-%d %H:%M:%S"`
    var_str="\n\033[32m${var_curr_timestamp} ${var_str}\033[0m"
    echo -e "${var_str}"
}

func_write_log "generate local start"
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
hwclock --systohc
# sed -i '/en_US.UTF-8/{s/#//}' /etc/locale.gen
sed -i '/en_GB.UTF-8/{s/#//}' /etc/locale.gen
locale-gen
echo 'LANG=zh_CN.UTF-8'  > /etc/locale.conf
echo "archlinux" > /etc/hostname
func_write_log "generate local end"


func_write_log "set user and password start"
func_write_log "please set the password of root"
#passwd root
echo root:${ROOTPASSWORD} | chpasswd
func_write_log "create user "${USER} 
useradd -m -g users -G wheel -s /bin/bash ${USER}
func_write_log "please set the password of "${USER}
# passwd ${USER}
echo ${USER}:${PASSWORD} | chpasswd
func_write_log "set user and password end"


func_write_log "install grub start"
pacman -S dosfstools grub efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg
func_write_log "install grub end"


func_write_log "install dhcpcd and other useful tools start"
pacman -S dhcpcd vim git wget curl net-tools dnsutils inetutils iproute2
systemctl enable dhcpcd
func_write_log "install dhcpcd and other useful tools end"


func_write_log "exit"
exit

