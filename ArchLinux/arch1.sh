#! /bin/bash

# name of disk, you should check and edit it 
DISK=/dev/sda
# size of disk, you should check and edit it
DISKSIZE=128G


function func_write_log()
{
    var_str=$1
    var_curr_timestamp=`date "+%Y-%m-%d %H:%M:%S"`
    var_str="\n\033[32m${var_curr_timestamp} ${var_str}\033[0m"
    echo -e "${var_str}"
}

func_write_log "make partation table start"  
parted ${DISK} mklabel gpt
parted ${DISK} mkpart fat32 2M 512M
parted ${DISK} mkpart ext4 512M ${DISKSIZE}
func_write_log "make partation table end"

func_write_log "format start" 
mkfs.fat ${DISK}1
mkfs.ext4 ${DISK}2
func_write_log "format end" 


func_write_log "mount start" 
mount ${DISK}2 /mnt
mkdir /mnt/boot /mnt/boot/efi
mount ${DISK}1 /mnt/boot/efi
func_write_log "mount end"


func_write_log "update pacman mirror list start" 
echo "Server = https://mirrors.tuna.tsinghua.edu.cn/archlinux/\$repo/os/\$arch" > /etc/pacman.d/mirrorlist
# echo "Server = http://mirrors.aliyun.com/archlinux/$repo/os/$arch" > /etc/pacman.d/mirrorlist
pacman -Syy
func_write_log "update pacman mirror end"


func_write_log "install the system start" 
pacstrap /mnt base linux linux-firmware
func_write_log "install the system end" 


func_write_log "genfstab start" 
genfstab -U /mnt >> /mnt/etc/fstab
func_write_log "genfstab end" 


func_write_log "chroot"
cp ./arch2.sh /mnt/arch2.sh
chmod +x /mnt/arch2.sh
arch-chroot /mnt /arch2.sh

func_write_log "umount"
umount -R /mnt

func_write_log "installation end, please reboot"
