class HexCellMap:
    def __init__(self, n):
        self.cells = dict()
        self.minx = 0
        self.miny = 0
        self.maxx = 0
        self.maxy = 0
        self.__build_map(n,21, 21)

    def __build_map(self, n, x, y):
        if self.cells.get((x, y), 0) >= n:
            return
        if n > 1:
            if n % 2 == 0:
                lis = [(0, 0), (1, 0), (0, 1), (-1, 1), (-1, 0), (-1, -1), (0, -1)]
            else:
                lis = [(0, 0), (1, -1), (1, 1), (0, -2), (0, 2), (-2, -1), (-2, 1)]
            length = 3 ** (n // 2 - 1)
            for i in lis:
                self.__build_map(n - 1, x + i[0] * length, y + i[1] * length)
        self.cells[(x, y)] = n
        self.maxx = max(self.maxx, x)
        self.maxy = max(self.maxy, y)
        self.minx = min(self.minx, x)
        self.miny = min(self.miny, y)


if __name__ == '__main__':
    mp = HexCellMap(6)
    print(mp.minx,mp.maxx,mp.miny,mp.maxy)

    lst = [[0 for i in range(50)] for j in range(50)]

    for i in mp.cells.items():
        # print(i)
        lst[i[0][0] ][i[0][1] ] = i[1]
    col = [' ', '-', '+', '*', '$', '@','@','@','@','@','@','@','@','@','@','@','@','@','@','@','@','@','@','@','@','@','@','@','@']
    for i in lst:
        for j in i:
            print(col[j], end='')
            print(' ', end='')
        print()

    # print(len(mp.cells))
