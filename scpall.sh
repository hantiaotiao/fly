#! /bin/bash

hosts=("host1" "host2" "host3" "host4")

if [ $# -ne 1 ]; then
  echo "usage: scpall.sh absolute_path"
else
  for host in "${hosts[@]}"; do
    cmd="scp -r "$1" user@"${host}":"$1
    echo $cmd
  done
fi
