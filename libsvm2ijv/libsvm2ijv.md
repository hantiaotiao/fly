## libsvm2ijv

一个把`libsvm`格式数据转为`ijv`格式数据的工具


### 用法

``` bash
spark-submit libsvm2ijv.jar path/to/input.svm path/to/output
```

`path/to/input.svm`是`hdfs`上的一个`libsvm`格式的文本文件

转换后的数据将被放着`path/to/output`目录中。
转换前不能有这个目录。
转换后该目录中会有四个文件： `a.ijv`， `b.ijv`， `a.ijv.mtd`， `b.ijv.mtd`。



### `libsvm`格式

`libsvm`使用的训练数据和检验数据文件格式如下：
```
 [label] [index1]:[value1] [index2]:[value2] …

 [label] [index1]:[value1] [index2]:[value2] …
```
`label`  目标值，就是说class（属于哪一类），就是你要分类的种类，通常是一些整数。

`index` 是有顺序的索引，通常是连续的整数。就是指特征编号，必须按照升序排列

`value` 就是特征值，用来训练的数据，通常是一堆实数组成。

输入文件，`input.svm`
```
0 1:1 2:1 3:1
1 1:0 2:0 3:0
0 1:5 3:5 4:5
1 3:1 7:1
```

### `ijv`格式

`ijv`格式的矩阵，每行存三个数，分别是： 行坐标，列坐标，值。

训练数据，`a.ijv`
```
1 1 1.0
1 2 1.0
1 3 1.0
2 1 0.0
2 2 0.0
2 3 0.0
3 1 5.0
3 3 5.0
3 4 5.0
4 3 1.0
4 7 1.0
```

标签，`b.ijv`
```
0 1 0.0
1 1 1.0
2 1 0.0
3 1 1.0
```