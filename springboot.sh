#!/bin/bash

port=8001

git pull
mvn -N io.takari:maven:wrapper -Dmaven=3.5.2
mvn clean package -Dmaven.test.skip=true
kill -9 $(lsof -ti tcp:$port)
nohup java -jar ./target/*.jar  &
