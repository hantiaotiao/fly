import pymysql

# 具有建库建用户权限的root账户
MYSQL_USER = "root"
MYSQL_PASSWORD = "123456"
MYSQL_HOST = "localhost"

# 欲创建的数据库和用户
DB_NAME = 'test12334'
DB_USER = 'test12334'
DB_PASSWORD = 'test134'
DB_HOST = '%' 


if __name__ == '__main__':
    
    connection = pymysql.connect(host=MYSQL_HOST,
                                user = MYSQL_USER,
                                password = MYSQL_PASSWORD)
    cursor = connection.cursor()

    # 因为玄学的引号，被迫使用字符串拼接的方式
    cursor.execute("CREATE DATABASE IF NOT EXISTS " + DB_NAME + ";") 
    cursor.execute("create user "+DB_USER+ "@'"+DB_HOST +"' identified by '"+ DB_PASSWORD+"';")
    cursor.execute("grant all privileges on "+DB_NAME+".* to "+DB_USER+"@'"+DB_HOST+"' identified by '"+DB_PASSWORD+"';")

    print('done')





