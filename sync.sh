#!/bin/bash

# hadoop spark 集群配置 一键同步脚本

hosts=("20s201" "20s204" "20s207" "20s208"  "20s210" "20s211" "20s213" "20s214")
user="hantiaotiao"


for host in ${hosts[*]}
do
  echo "#######################################"
  echo "#                                     "
  echo "#           sync ${host}              "
  echo "#                                     "
  
  rsync -a  --exclude "spark-2.4.5/work/*" --exclude "spark-2.4.5/logs/*" --exclude "hadoop-2.9.2/logs/*" --progress --stats -v  ~/env ${user}@${host}:~/
  rsync -a  --progress  --stats -v   ~/.bashrc  ${user}@${host}:~/

done
